%{
"Hierarchical pedestrian abnormal trajectory detection using human tracking
and background information"

This Matlab program is the implementation part of the paper: 
Doan Trung Nghia, Sunwoong Kim, Vo Le Cuong and Hyuk-Jae Lee, 
"Anomalous Trajectory Detection in Surveillance Systems Using Pedestrian 
and Surrounding Information", to be submitted to IEIE SPC journal 

Please refer to the paper for more details. 

Matlab R2015 is required since this paper makes use of the built in Machine
Learning tool box.

Author          :   Doan Trung Nghia 
Research lab    :   Computer Architecture and Parallel Processing (CAPP),
                    Seoul National University, Korea
Date            :   2016, March 15th
%}

%% Setup process
%{
    Run Setup.m to download and compile the prerequisite programs.
%}

%% Main program
%{
    Run AbnormalTrackDetection.m to get demo results
%}

% Please read the paper for more details.
% 